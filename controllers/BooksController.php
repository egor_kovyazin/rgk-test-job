<?php

namespace app\controllers;

use app\models\common\Books;
use app\models\common\Authors;
use app\models\frontend\SearchBookForm;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use \yii\web\HttpException;
use \yii\helpers\Url;

class BooksController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['create', 'view', 'update', 'delete'],
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['create', 'view', 'update', 'delete'],
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchBook = new SearchBookForm();
        $searchBook->load(Yii::$app->getRequest()->get());

        Url::remember();

        return $this->render('index', [
            'searchForm' => $searchBook,
        ]);
    }

    public function actionCreate()
    {
        $model = new Books(null);

        if ($model->load(Yii::$app->getRequest()->post()) && $model->save()) {
            Yii::$app->session->setFlash('bookFlash', 'Book "'. $model->name .'" ['. $model->id .']  created.');
            return $this->redirect(['index']);
        }

        return $this->render('create', ['model' => $model]);
    }

    public function actionUpdate($id)
    {
        $urlKey = 'to_book_list_from_'.$id;

        if (empty(Url::previous($urlKey))) {
            $prevUrl = (Url::previous() == Url::current()) ? ['/books/index'] : Url::previous();
            Url::remember($prevUrl, $urlKey);
        }

        $model = $this->loadModel($id);
        $request = Yii::$app->getRequest();
        if ($model->load($request->post()) && $model->save()) {
            Yii::$app->session->setFlash('bookFlash', 'Book "'. $model->name .'" ['. $model->id .'] updated.');
            return $this->redirect(Url::previous($urlKey));
        }

        return $this->render('update', ['model' => $model]);
    }

    public function actionView($id)
    {
        if (!Yii::$app->getRequest()->isAjax) {
            throw new HttpException(400, 'Support only Ajax request.');
        }

        $model = $this->loadModel($id);
        return $this->renderAjax('view', ['model' => $model]);
    }

    public function actionDelete($id)
    {
        $model = $this->loadModel($id);

        $id = $model->id;
        $name = $model->name;

        $model->delete();
        Yii::$app->session->setFlash('bookFlash', 'Book "'. $name .'" ['. $id .'] removed.');

        return $this->redirect(['index']);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param int $id the $integer ID of the model to be loaded
     * @throws \yii\web\HttpException
     * @return Books
     */
    protected function loadModel($id)
    {
        /** @var Books $model */
        $model = Books::findOne($id);
        if ($model === null) {
            throw new HttpException(404, 'The requested page does not exist.');
        }

        return $model;
    }

    public function getAuthorsList()
    {
        $source = Authors::find()->asArray()->all();
        $list = [];
        foreach ($source as $author) {
            $list[$author['id']] = $author['firstname'] .' '. $author['lastname'];
        }

        return $list;
    }
}
