$(function() {
    initImageAnimated();
    initViewBookAction();
    initViewEditableImage();
});

function initImageAnimated() {
    $('.js-image-animated').on('click', function(){
        var self = $(this);
        var originWidth = self.width();
        var increaseWidth = originWidth + 100;

        self.animate({width: ''+ increaseWidth +'px'}, 500, function(){
          self.animate({width: ''+ originWidth +'px'});
        });
    });
}

function initViewBookAction() {
    $('.js-view-book').on('click', function() {
        var self = $(this);
        var modal = $('#view-book-modal');

        $.get('/books/view', {id: self.attr('data-id')}).done(function( data ) {
            modal.find('.modal-title')
                .html('Detail book "' + self.attr('data-name') + '" [' + self.attr('data-id') + ']');

            modal.find('.modal-body').html(data);
        });
    });
}

function initViewEditableImage() {
    $('.js-image-src-url').on('blur', function() {
        $(this).closest('form').find('.js-image-url').attr('src', $(this).val());
    });
}