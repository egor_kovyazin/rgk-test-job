package "graphviz"

bash "install-xhprof" do
  not_if {File.directory?(node["app"]["server_root"] + "/xhprof")}
  cwd node["app"]["server_root"]
  code <<-EOH

    #wget http://pecl.php.net/get/xhprof-0.9.2.tgz
    wget https://github.com/facebook/xhprof/archive/master.zip
    unzip master.zip
    cd xhprof-master/extension/
    phpize
    ./configure
    make
    make install

    echo -e 'extension=xhprof.so\nxhprof.output_dir="#{node["app"]["server_root"]}/xhprof/output"' > /etc/php5/fpm/conf.d/xhprof.ini

    cd ../../
    mv xhprof-master xhprof
    mkdir xhprof/output
    rm master.zip
    rm -rf xhprof-master

    chown -R #{node["app"]["user"]}:#{node["app"]["group"]} xhprof

  EOH
  notifies :restart, "service[php5-fpm]", :delayed
end

template "/etc/nginx/sites-enabled/xhprof.conf" do
  source "nginx/xhprof.conf.erb"
  owner "root"
  group "root"
  mode 0644
  notifies :reload, "service[nginx]", :delayed
end
