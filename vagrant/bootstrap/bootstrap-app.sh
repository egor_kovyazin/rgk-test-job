#!/usr/bin/env bash


##### Configure #####

# Create directories
if [ ! -d /home/vagrant/tmp ]; then
  mkdir /home/vagrant/tmp
  chown vagrant:vagrant /home/vagrant/tmp
fi
if [ ! -d /home/vagrant/logs ]; then
  mkdir /home/vagrant/logs
  chown vagrant:vagrant /home/vagrant/logs
fi

# Replace nginx configuration file
if [ -f /etc/nginx/sites-enabled/default ]; then
  rm /etc/nginx/sites-enabled/default
fi
ln -sfn /vagrant/bootstrap/config/nginx.conf /etc/nginx/sites-enabled/app.conf
service nginx restart

# Replace php-fpm configuration file
if [ -f /etc/php5/fpm/pool.d/www.conf ]; then
  rm /etc/php5/fpm/pool.d/www.conf
fi
ln -sfn /vagrant/bootstrap/config/fpm.conf /etc/php5/fpm/pool.d/www.conf
service php5-fpm restart

# Update dependances
cd /home/vagrant/app
sudo -u vagrant composer global require "fxp/composer-asset-plugin:~1.0.0"
sudo -u vagrant composer install

# TODO: hack
if ! [ -d /home/vagrant/app/vendor/bower ]; then
  sudo -u vagrant ln -sfn /home/vagrant/app/vendor/bower-asset /home/vagrant/app/vendor/bower
fi

# Init the database with rudimentary init data
cd /home/vagrant/app
./yii migrate --interactive=0