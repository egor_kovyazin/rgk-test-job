#!/usr/bin/env bash

##### Create MySQL databases #####

MY_USER=yii-db-user
MY_PASS=yii-db-pass

for db in main_db
do
    if ! mysqlshow -u root | grep -q $db
    then
        mysql -u root -e "CREATE DATABASE $db CHARACTER SET utf8 COLLATE utf8_general_ci;"
        mysql -u root -e "grant all on $db.* to '$MY_USER'@'%' identified by '$MY_PASS';"
        mysql -u root -e "grant all on $db.* to '$MY_USER'@'localhost' identified by '$MY_PASS';"
    fi
done
