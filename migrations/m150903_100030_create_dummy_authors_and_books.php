<?php

use yii\db\Migration;

class m150903_100030_create_dummy_authors_and_books extends Migration {

    public function safeUp() {

        /**
         * Insert authors
         */
        $this->insert('authors', [
            'id'        => 1,
            'firstname' => 'Иван-1',
            'lastname'  => 'Иванов-1',
        ]);

        $this->insert('authors', [
            'id'        => 2,
            'firstname' => 'Петр-2',
            'lastname'  => 'Питров-2',
        ]);

        $this->insert('authors', [
            'id'        => 3,
            'firstname' => 'Сидор-3',
            'lastname'  => 'Сидоров-3',
        ]);

        /**
         * Insert books
         */
        $this->insert('books', [
            'id'          => '1',
            'name'        => 'Книга 1 Иванова',
            'date_create' => '2015-06-07 00:00:00',
            'date_update' => '2015-06-08 00:00:00',
            'preview'     => 'http://png-images.ru/wp-content/uploads/2015/01/book_PNG2116.png',
            'date'        => '2014-06-01',
            'author_id'   => 1,
        ]);

        $this->insert('books', [
            'id'          => '2',
            'name'        => 'Книга 2 Петрова',
            'date_create' => '2015-05-07 00:00:00',
            'date_update' => '2015-05-08 00:00:00',
            'preview'     => 'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT20ScTQ7mMnDjoMD9jZ0rEv2Bgb66HKv01LDAYDS66nnqeGRbGNA',
            'date'        => '2013-06-01',
            'author_id'   => 2,
        ]);

        $this->insert('books', [
            'id'          => '3',
            'name'        => 'Книга 3 Иванова',
            'date_create' => '2014-05-07 00:00:00',
            'date_update' => '2014-05-08 00:00:00',
            'preview'     => 'http://1.bp.blogspot.com/-qJA7dW3W-M0/VH4l2LQUIEI/AAAAAAAAACo/ZJqeqHgpmBs/s1600/books.png',
            'date'        => '2012-06-01',
            'author_id'   => 1,
        ]);

        $this->insert('books', [
            'id'          => '4',
            'name'        => 'Книга 4 Сидорова',
            'date_create' => '2014-01-07 00:00:00',
            'date_update' => '2014-01-08 00:00:00',
            'preview'     => 'http://mamabox.ru/wp-content/uploads/2012/12/book.png',
            'date'        => '2010-06-01',
            'author_id'   => 3,
        ]);

        $this->insert('books', [
            'id'          => '5',
            'name'        => 'Книга 5 Сидорова',
            'date_create' => '2014-01-07 00:00:00',
            'date_update' => '2014-01-08 00:00:00',
            'preview'     => 'http://mamabox.ru/wp-content/uploads/2012/12/book.png',
            'date'        => '2011-07-01',
            'author_id'   => 3,
        ]);

        $this->insert('books', [
            'id'          => '6',
            'name'        => 'Книга 6 Сидорова',
            'date_create' => '2014-01-07 00:00:00',
            'date_update' => '2014-01-08 00:00:00',
            'preview'     => 'http://mamabox.ru/wp-content/uploads/2012/12/book.png',
            'date'        => '2013-07-01',
            'author_id'   => 3,
        ]);

        $this->insert('books', [
            'id'          => '7',
            'name'        => 'Книга 7 Сидорова',
            'date_create' => '2014-01-07 00:00:00',
            'date_update' => '2014-01-08 00:00:00',
            'preview'     => 'http://mamabox.ru/wp-content/uploads/2012/12/book.png',
            'date'        => '2013-01-01',
            'author_id'   => 3,
        ]);

        $this->insert('books', [
            'id'          => '8',
            'name'        => 'Книга 8 Сидорова',
            'date_create' => '2014-01-07 00:00:00',
            'date_update' => '2014-01-08 00:00:00',
            'preview'     => 'http://1.bp.blogspot.com/-qJA7dW3W-M0/VH4l2LQUIEI/AAAAAAAAACo/ZJqeqHgpmBs/s1600/books.png',
            'date'        => '2013-01-09',
            'author_id'   => 3,
        ]);

        $this->insert('books', [
            'id'          => '9',
            'name'        => 'Книга 9 Сидорова',
            'date_create' => '2014-01-07 00:00:00',
            'date_update' => '2014-01-08 00:00:00',
            'preview'     => 'http://mamabox.ru/wp-content/uploads/2012/12/book.png',
            'date'        => '2013-01-01',
            'author_id'   => 3,
        ]);

        $this->insert('books', [
            'id'          => '10',
            'name'        => 'Книга 10 Сидорова',
            'date_create' => '2014-01-07 00:00:00',
            'date_update' => '2014-01-08 00:00:00',
            'preview'     => 'http://1.bp.blogspot.com/-qJA7dW3W-M0/VH4l2LQUIEI/AAAAAAAAACo/ZJqeqHgpmBs/s1600/books.png',
            'date'        => '2013-01-09',
            'author_id'   => 3,
        ]);

        $this->insert('books', [
            'id'          => '11',
            'name'        => 'Книга 11 Сидорова',
            'date_create' => '2014-01-07 00:00:00',
            'date_update' => '2014-01-08 00:00:00',
            'preview'     => 'http://1.bp.blogspot.com/-qJA7dW3W-M0/VH4l2LQUIEI/AAAAAAAAACo/ZJqeqHgpmBs/s1600/books.png',
            'date'        => '2013-01-09',
            'author_id'   => 3,
        ]);

        $this->insert('books', [
            'id'          => '12',
            'name'        => 'Книга 12 Сидорова',
            'date_create' => '2014-01-07 00:00:00',
            'date_update' => '2014-01-08 00:00:00',
            'preview'     => 'http://mamabox.ru/wp-content/uploads/2012/12/book.png',
            'date'        => '2013-01-01',
            'author_id'   => 3,
        ]);

        $this->insert('books', [
            'id'          => '13',
            'name'        => 'Книга 13 Сидорова',
            'date_create' => '2014-01-07 00:00:00',
            'date_update' => '2014-01-08 00:00:00',
            'preview'     => 'http://1.bp.blogspot.com/-qJA7dW3W-M0/VH4l2LQUIEI/AAAAAAAAACo/ZJqeqHgpmBs/s1600/books.png',
            'date'        => '2013-01-09',
            'author_id'   => 3,
        ]);
    }

    public function safeDown() {
        $this->truncateTable('books');
        $this->truncateTable('authors');
    }
}
