<?php

use yii\db\Migration;

class m150903_100010_create_books_table extends Migration {

    /**
     * @todo поля "date_create" и "date_update" правильнее именовать "created_at" и "updated_at"
     */
    public function up() {
        $this->createTable('books', [
            'id'          => 'int(11) unsigned NOT NULL AUTO_INCREMENT',
            'name'        => 'varchar(255) NOT NULL',
            'date_create' => 'datetime DEFAULT NULL',
            'date_update' => 'datetime DEFAULT NULL',
            'preview'     => 'text DEFAULT NULL',
            'date'        => 'date DEFAULT NULL',
            'author_id'   => 'int(11) DEFAULT NULL',
            'PRIMARY KEY (`id`)',
        ]);
    }

    public function down() {
        $this->dropTable('books');
    }
}
