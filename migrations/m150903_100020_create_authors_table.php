<?php

use yii\db\Migration;

class m150903_100020_create_authors_table extends Migration {

    public function up() {
        $this->createTable('authors', [
            'id'        => 'int(11) unsigned NOT NULL AUTO_INCREMENT',
            'firstname' => 'varchar(255) NOT NULL',
            'lastname'  => 'varchar(255) NOT NULL',
            'PRIMARY KEY (`id`)',
        ]);
    }

    public function down() {
        $this->dropTable('authors');
    }
}
