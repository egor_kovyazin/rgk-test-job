<?php
/**
 * @var yii\web\View $this
 * @var app\models\common $model
 */

$this->title = 'Create book';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this-> render('_form', ['model' => $model]); ?>
