<?php
/**
 * @var yii\web\View $this
 * @var app\models\common $model
 */

use yii\helpers\Html;

$this->title = 'Edit "'. Html::encode($model->name) .'" ['. $model->id .']';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', ['model' => $model]); ?>
