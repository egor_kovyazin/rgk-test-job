<?php
/**
 * @var $this yii\web\View
 * @var app\models\frontend\SearchBookForm $model
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>

<div class="site-index">
    <div class="body-content">

        <?php $form = ActiveForm::begin([
            'id'     => 'search-form',
            'method' => 'get',
            'layout' => 'horizontal',
        ]); ?>

            <?= $form->field($model, 'author_id')->dropDownList($this->context->getAuthorsList(), ['prompt'=>'']) ?>
            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'date_from') ?>
            <?= $form->field($model, 'date_to') ?>

        <divright">
            <?= Html::submitButton('Искать', ['class' => 'btn btn-primary', 'name' => 'search-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>


