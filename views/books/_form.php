<?php
/**
 * @var yii\web\View $this
 * @var app\models\common $model
 */

use app\models\common\Authors;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$sourceAuthors = Authors::find()->asArray()->all();
$authors = [];
foreach ($sourceAuthors as $author) {
    $authors[$author['id']] = $author['firstname'] .' '. $author['lastname'];
}

?>

<div class="site-index">
    <div class="body-content">

        <?php $form = ActiveForm::begin(['id' => 'books-form']); ?>

            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'date_create')->textInput(['readonly' => true]) ?>
            <?= $form->field($model, 'date_update')->textInput(['readonly' => true]) ?>
            <?= $form->field($model, 'preview')->textArea(['rows' => 2, 'class' => 'form-control js-image-src-url']) ?>

            <?= Html::img($model->preview, ['width' => '100px', 'class' => 'js-image-url']) ?>

            <?= $form->field($model, 'date') ?>
            <?= $form->field($model, 'author_id')->dropDownList($authors, ['prompt'=>'']) ?>

        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default', 'name' => 'reset-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
