<?php
/**
 * @var $this yii\web\View
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\frontend\SearchBookForm $searchForm
 */

use Yii;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Books';

?>

<div class="site-index">
    <div class="body-content">

        <?= $this->render('_search', ['model' => $searchForm]) ?>

        <?php if (Yii::$app->session->hasFlash('bookFlash')): ?>
        <div class="alert alert-success">
            <?= Yii::$app->session->getFlash('bookFlash'); ?>
        </div>
        <?php endif; ?>

        <div class="clearfix">
            <blockquote class="pull-right">
                <small><?= Html::a('Create book', ['/books/create'])?></small>
            </blockquote>
        </div>

        <?= GridView::widget([
            'dataProvider' => $searchForm->search(),
            'columns' => [
                'id',
                'name',
                [
                    'attribute' => 'preview',
                    'format'    => 'html',
                    'value'     => function ($data) {
                        return Html::img($data->preview, [
                            'width' => '50px', 
                            'class' => 'js-image-animated',
                        ]);
                    },
                ],
                [
                    'label'     => 'Автор',
                    'attribute' => 'author.firstname',
                    'value'     => function ($data) {
                        return $data->author->fullname;
                    },
                ],
                'date',
                'date_create',
                 [
                'header'   => 'Действия',
                'class'    => 'yii\grid\ActionColumn',
                'template' => (!Yii::$app->user->isGuest) ? '{view}  {update}  {delete}': '',
                'buttons'  => [
                    'view' => function ($url, $model, $key) {
                       return Html::a('<span class="glyphicon glyphicon-eye-open"></span>','#', [
                           'class'       => 'js-view-book',
                           'data-toggle' => 'modal',
                           'data-target' => '#view-book-modal',
                           'data-id'     => $key,
                           'data-name'   => $model->name,
                       ]);
                    },
                ],
            ],
            ],
        ]); ?>

        <?= $this->render('_modal', ['id' => 'view-book-modal']) ?>

    </div>
</div>
