<?php
/**
 * @var yii\web\View $this
 * @var app\models\common\Books $model
 */

use yii\widgets\DetailView;
?>

<?= DetailView::widget([
    'model'     => $model,
    'attributes' => [
        [
            'attribute' => 'preview',
            'value'     => $model->preview,
            'format' => ['image', ['width'=>'50px']],
        ],
        'author.fullname',
        'date',
        'date_create',
    ],
]); ?>
