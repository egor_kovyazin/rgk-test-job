<?php
/**
 * @var yii\web\View $this
 * @var string $id
 */

use yii\bootstrap\Modal;

?>

<?php Modal::begin([
    'id'     => ($id ?: 'view-book-modal'),
    'header' => '<h4 class="modal-title">View book</h4>',
    'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Close</a>',
]); ?>

<div class="modal-body well">
</div>

<?php Modal::end(); ?>