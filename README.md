Test project on Yii 2
============================

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.



RUN UNDER VAGRANT
-------------

### Requirements for developing

    * VirtualBox >= 4.3.10 and Extension Pack
    * Vagrant >= 1.7.2
    * Account on https://atlas.hashicorp.com


### Quick start

    mkdir ~/sites/app && cd ~/sites/app
    
    git clone git@bitbucket.org:egor_kovyazin/rgk-test-job.git app && cd app \
    && git submodule sync && git submodule init && git submodule update \
    && cd vagrant \
    && vagrant plugin install vagrant-omnibus && vagrant plugin install vagrant-vbguest && vagrant up
    
    echo -e '\n# Test project\n171.15.36.10 rgk-test.local' >> /etc/hosts


When the vagrant environment is up, you'll be able to access http://rgk-test.local in your browser
or 171.15.36.10:3306 in your MySQL client (if you don't use Sequel Pro yet, you definitely should).
