<?php

return [
    'adminEmail' => 'admin@rgk.com',
    'cacheDuration' => 120, // seconds
];
