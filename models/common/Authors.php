<?php

namespace app\models\common;

use yii\db\ActiveRecord;

/**
 * Model for table "Authors"
 *
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 * 
 * @property integer $id
 * @property string  $firstname
 * @property string  $lastname
 * @property \yii\db\ActiveQuery $books
 * @property string $fullname {$link Authors::getFullname()}
 */
class Authors extends ActiveRecord
{
    /**
     * Relation for "Books"
     * @return ActiveQuery
     */
    public function getBooks()
    {
        return $this->hasMany(Books::className(), ['author_id' => 'id']);
    }

    public function getFullname()
    {
        return $this->firstname .' '. $this->lastname;
    }
}
