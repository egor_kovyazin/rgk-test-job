<?php

namespace app\models\common;

use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * Model for table "Books"
 *
 * @author Egor Kovyazin <egor.koviazin@gmail.com>
 * 
 * @property integer $id
 * @property string  $name
 * @property string  $date_create (format: Y-m-d H:i:s)
 * @property string  $date_update (format: Y-m-d H:i:s)
 * @property string  $preview
 * @property string  $date (format: Y-m-d H:i:s)
 * @property integer $author_id
 * @property \yii\db\ActiveQuery $author
 */
class Books extends ActiveRecord
{

    public function rules()
    {
        return [
            [['name'], 'required'],
            ['preview', 'url', 'defaultScheme' => ''],
            ['date', 'date', 'format' => 'yyyy-M-d'],
            ['author_id', 'exist', 'targetClass' => Authors::className(), 'targetAttribute' => 'id'],

            //['date_update', 'default', 'value' => gmdate("Y-m-d H:i:s")],
        ];
    }

    /**
     * Relation for "Authors"
     * @return ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Authors::className(), ['id' => 'author_id']);
    }

    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'name'        => 'Название',
            'preview'     => 'Превью',
            'date'        => 'Дата выхода книги',
            'date_create' => 'Дата добавления',
        ];
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), ['author.firstname']);
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->date_create = gmdate('Y-m-d H:i:s');
        }
        $this->date_update = gmdate('Y-m-d H:i:s');

        return parent::beforeSave($insert);
    }
}
