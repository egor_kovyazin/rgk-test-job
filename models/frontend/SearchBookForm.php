<?php

namespace app\models\frontend;

use app\models\common\Books;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class SearchBookForm extends Model
{
    public $author_id;
    public $name;
    public $date_from;
    public $date_to;

    public function attributeLabels()
    {
        return [
            'author_id' => 'Автор',
            'name'      => 'Название',
            'date_from' => 'Дата выхода книги (от)',
            'date_to'   => 'Дата выхода книги (до)',
        ];
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['author_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['date_from', 'date_to'], 'filter', 'filter' => [$this, 'filterDate']],
        ];
    }

    /**
     * Filter date.
     * @param string $value the source value being filtered
     */
    public function filterDate($value)
    {
        if (empty($value)) {
            return $value;
        }

        return preg_replace("/(\d+)\D+(\d+)\D+(\d+)/","$3-$2-$1", $value);
    }

    /**
     * Search books.
     * @return \app\models\common\ActiveDataProvider
     */
    public function search()
    {
        $query = Books::find();
        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => ['pageSize' => 5],
        ]);

        $this->validate();

        $query->andFilterWhere(['=', 'author_id', $this->author_id]);
        $query->andFilterWhere(['like', 'name', $this->name]);
        if (!empty($this->date_from)) {
            $query->andFilterWhere(['>=', 'date', $this->date_from]);
        }
        if (!empty($this->date_to)) {
            $query->andFilterWhere(['<=', 'date', $this->date_to]);
        }

        $query->joinWith(['author' => function($query) { $query->from(['author' => 'authors']); }]);

        return $dataProvider;
    }
}
